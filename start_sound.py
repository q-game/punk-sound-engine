#! ../env/bin/python
# encoding: utf-8
import os
import glob
import json
import signal
import logging
from pprint import pprint

import zmq

from sound import Sound, Playlist, jackcli
from sound import SoundError


zmq_ctx = zmq.Context.instance()

STATUS = {
    'stop': False
}

def host(*args):
    return '0.0.0.0'

ADRS = {
    'SND': {
        'ip': host('qg_punk_snd'),
        'port': '50000'
    },
}

log = logging.getLogger('BAL')
log.setLevel(logging.DEBUG)
console_handler = logging.StreamHandler()
log.addHandler(console_handler)


def start_sound(handler, playlist):
    #import pudb; pu.db
    try:
        address = "tcp://{ip}:{port}".format(**ADRS['SND'])
        sock = zmq_ctx.socket(zmq.REP)
        sock.setsockopt(zmq.LINGER, 1000)
        sock.bind(address)

        def shutdown(signal, frame):
            playlist.stop_all()
            sock.close()
            log.info("Termination of zmq context...")
            zmq_ctx.destroy()
            disconnect_mic()
            sys.exit(0)
        signal.signal(signal.SIGTERM, shutdown)

        poller = zmq.Poller()
        poller.register(sock, zmq.POLLIN)
        log.info('SND started!')
        while not STATUS['stop']:
            connect = poller.poll(100)
            if connect:
                request = json.loads(sock.recv())
                try:
                    log.debug("RespServer: New request! {0}".format(request))
                    resp = handler(playlist, request)
                except Exception as e:
                    resp = {"status": "fail", "info": "Exception in handler"}
                    log.debug("Exception in handler: {0}".format(e))
                    #raise e

                # Achtung: only for debug!!!
                #resp = {"status": "success", "info": "Debug mode"}
                sock.send(json.dumps(resp))
                log.debug("RespServer: Send response! {0}".format(resp))
            else:
                pass
    except (KeyboardInterrupt, SystemExit):
        log.info("System interrupt...")

    finally:
        sock.close()
        log.info("Termination of zmq context...")
        zmq_ctx.destroy()


def handler(playlist, request):
    if request.get('type') == 'system':
        if request.get('cmd') == 'shutdown':
            playlist.stop_all()
            STATUS['stop'] = True
            return {
                "status": "success"
            }

    elif request.get('type') == 'cmd':
        if request.get('cmd') == 'play':
            full_name = request.get('full_name')
            routes = request.get('routes', None)
            volume = request.get('volume', None)
            loop = request.get('loop', None)
            paused = request.get('paused', None)
            try:
                playlist.play(full_name, routes, volume, loop, paused)
            except SoundError as err:
                return {
                    "status": "fail",
                    "full_name": full_name,
                    "info": err.message
                }
            else:
                return {
                    "status": "success",
                    "full_name": full_name
                }

        elif request.get('cmd') == 'stop':
            full_name = request.get('full_name')
            try:
                playlist.stop(full_name)
            except SoundError as err:
                return {
                    "status": "fail",
                    "full_name": full_name,
                    "info": err.message
                }
            else:
                return {
                    "status": "success",
                    "full_name": full_name
                }

        elif request.get('cmd') == 'resume':
            full_name = request.get('full_name')
            try:
                playlist.resume(full_name)
            except SoundError as err:
                return {
                    "status": "fail",
                    "full_name": full_name,
                    "info": err.message
                }
            else:
                return {
                    "status": "success",
                    "full_name": full_name
                }

        elif request.get('cmd') == 'pause':
            full_name = request.get('full_name')
            try:
                playlist.pause(full_name)
            except SoundError as err:
                return {
                    "status": "fail",
                    "full_name": full_name,
                    "info": err.message
                }
            else:
                return {
                    "status": "success",
                    "full_name": full_name
                }

        elif request.get('cmd') == 'route':
            full_name = request.get('full_name')
            routes = request.get('routes')
            try:
                playlist.route(full_name, routes)
            except SoundError as err:
                return {
                    "status": "fail",
                    "full_name": full_name,
                    "info": err.message
                }
            else:
                return {
                    "status": "success",
                    "full_name": full_name
                }

        elif request.get('cmd') == 'unroute':
            full_name = request.get('full_name')
            routes = request.get('routes')
            try:
                playlist.unroute(full_name, routes)
            except SoundError as err:
                return {
                    "status": "fail",
                    "full_name": full_name,
                    "info": err.message
                }
            else:
                return {
                    "status": "success",
                    "full_name": full_name
                }

        elif request.get('cmd') == 'checkcpu':
            try:
                playlist.check_cpu()
            except SoundError as err:
                return {
                    "status": "fail",
                    "info": err.message
                }
            else:
                return {
                    "status": "success",
                }

        else:
            return {
                "status": "fail",
                "info": "No such command: %s" % request.get('cmd')
            }


def add_sounds(playlist, dir_path, post_fix=''):
    sounds_dir = os.path.join(os.getcwd(), dir_path)
    for sound_file_path in glob.glob(os.path.join(sounds_dir, "*.wav")):
        playlist.add(sound_file_path, post_fix)

def connect_mic():
    for src in [1,2]:
        for sink in [1,2,3,4,5,6,7,8]:
            src_port = "system:capture_{0}".format(src)
            sink_port = "system:playback_{0}".format(sink)
            jackcli.connect(src_port, sink_port)

def disconnect_mic():
    for src in [1,2]:
        for sink in [1,2,3,4,5,6,7,8]:
            src_port = "system:capture_{0}".format(src)
            sink_port = "system:playback_{0}".format(sink)
            jackcli.disconnect(src_port, sink_port)



if __name__ == '__main__':
    playlist = Playlist()
    add_sounds(playlist, '../../sounds/all_sounds_wav/')
    add_sounds(playlist, '../../sounds/all_sounds_wav/english/', 'eng')
    pprint(playlist.sounds)
    playlist.add('../../sounds/all_sounds_wav/ochered.wav', 1)
    playlist.add('../../sounds/all_sounds_wav/ochered.wav', 2)
    playlist.add('../../sounds/all_sounds_wav/ochered.wav', 3)
    playlist.add('../../sounds/all_sounds_wav/ochered.wav', 4)
    playlist.add('../../sounds/all_sounds_wav/osechka.wav', 1)
    playlist.add('../../sounds/all_sounds_wav/osechka.wav', 2)
    playlist.add('../../sounds/all_sounds_wav/osechka.wav', 3)
    playlist.add('../../sounds/all_sounds_wav/osechka.wav', 4)
    playlist.add('../../sounds/all_sounds_wav/sbros_bombi.wav', 1)
    playlist.add('../../sounds/all_sounds_wav/sbros_bombi.wav', 2)
    playlist.add('../../sounds/all_sounds_wav/sbros_bombi.wav', 3)
    playlist.add('../../sounds/all_sounds_wav/sbros_bombi.wav', 4)
    playlist.add('../../sounds/all_sounds_wav/v_nas_popali.wav', 1)
    playlist.add('../../sounds/all_sounds_wav/v_nas_popali.wav', 2)
    playlist.add('../../sounds/all_sounds_wav/v_nas_popali.wav', 3)
    playlist.add('../../sounds/all_sounds_wav/v_nas_popali.wav', 4)

    connect_mic()
    start_sound(handler, playlist)
    disconnect_mic()
