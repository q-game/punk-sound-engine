# encode: utf-8
from gi.repository import Gst
import sys
import threading
from os import path
import os
import glob

from utils import Element, Pipeline, Bin
from utils import link, flags, msg_str
from utils import debug_graph
debug_graph.init()

class Sound(object):
    def __init__(self, file_path):
        self.name = path.basename(file_path).split('.')[0]

        self.bin = Bin(self.name)
        self.el_src = Element('filesrc', 'src', location=file_path)
        self.el_parse = Element('wavparse', 'parse')
        self.el_convert = Element('audioconvert', 'convert')
        self.el_resample = Element('audioresample', 'resample')

        self.bin.add(self.el_src, self.el_parse, self.el_convert, self.el_resample)
        link(self.el_src, self.el_parse, self.el_convert, self.el_resample)

        pad = self.el_resample.get_static_pad('src')
        ghost_pad = Gst.GhostPad('src', pad)
        ghost_pad.set_active(True)
        self.bin.add_pad(ghost_pad)

        self.output = ghost_pad

class JackOutput(object):
    def __init__(self, name):
        self.name = name
        self.el_sink = Element('jackaudiosink', 'puten', connect=0, client_name='put')
        self.input = self.el_sink.get_static_pad('sink')

class AlsaOutput(object):
    def __init__(self, device='hw:CARD=PCH,DEV=0', channels=2):
        self.name = device
        self.channels = channels

        self.bin = Bin(self.name)
                                                            # \/ SUPER IMPORTANT!!!
        self.el_interleave = Element('interleave', 'merge', channel_positions=[0,1])
        self.el_convert = Element('audioconvert', 'convert')
        self.el_resample = Element('audioresample', 'resample')
        self.el_sink = Element('alsasink', 'audio_sink', device=device)
        self.bin.add(self.el_interleave, self.el_convert, self.el_resample, self.el_sink)
        link(self.el_interleave, self.el_convert, self.el_resample, self.el_sink)

        for n in range(1, channels+1):
            channel = 'channel_{0}'.format(n)
            pad = self.el_interleave.get_request_pad('sink_%u')
            ghost_pad = Gst.GhostPad(channel, pad)
            ghost_pad.set_active(True)
            self.bin.add_pad(ghost_pad)
            setattr(self, channel, ghost_pad)


def create_sounds():
    sounds = dict()
    sounds_dir = os.path.join(os.getcwd(), 'sounds/')
    for sound_file_path in glob.glob(os.path.join(sounds_dir, "*.wav")):
        sound = Sound(sound_file_path)
        if sound.name not in sounds:
            sounds[sound.name] = sound
        else:
            raise Exception("Sound names collision: {0}".format(sound.name))
    return sounds

def test_alsa():
    Gst.init(sys.argv)

    sounds = create_sounds()
    put = sounds['put']
    chel = sounds['chel']

    alsa = AlsaOutput()

    pipeline = Pipeline('test-pipeline')

    pipeline.add(
        put.bin,
        chel.bin,
        alsa.bin
    )

    put.output.link(alsa.channel_1)
    chel.output.link(alsa.channel_2)

    debug_graph(pipeline, 'pre')
    pipeline.set_state('playing')

    bus = pipeline.get_bus()
    msg = bus.timed_pop_filtered(
        Gst.CLOCK_TIME_NONE,
        flags('MessageType', *'error eos'.split())
    )
    debug_graph(pipeline, 'post')
    print msg_str(msg)

    pipeline.set_state('null')


def test_jack():
    Gst.init(sys.argv)

    sounds = create_sounds()
    put = sounds['put']
    #chel = sounds['chel']

    jack1 = JackOutput('put')
    #jack2 = JackOutput('chel')

    pipeline = Pipeline('test-pipeline')

    pipeline.add(
        put.bin,
        #chel.bin,
        jack1.el_sink,
        #jack2.el_sink
    )

    put.output.link(jack1.input)
    #chel.output.link(jack2.input)

    debug_graph(pipeline, 'pre')
    pipeline.set_state('playing')
    debug_graph(pipeline, 'post')

    bus = pipeline.get_bus()
    msg = bus.timed_pop_filtered(
        Gst.CLOCK_TIME_NONE,
        flags('MessageType', *'error eos'.split())
    )
    print msg_str(msg)

    pipeline.set_state('null')


def main():
    test_jack()


main()

