# encode: utf-8
import types

from gi.repository import Gst
from gi.module import FunctionInfo

class QueryDict(dict):
    '''QueryDict is a dict() that can be queried with dot.'''
    def __getattr__(self, attr):
        return self.__getitem__(attr)

    def __setattr_(self, attr, value):
        self.__setitem__(attr, value)
qdict = QueryDict

def _check_gst_inited():
    if not Gst.is_initialized():
        raise GstNotInited()

def _set_props(obj, **props):
    for prop, val in props.iteritems():
        obj.set_property(prop.replace('_', '-'), val)
    return obj


class GstException(Exception): pass
class GstNotInited(GstException): pass
class ElementNotFound(GstException): pass

def create_element(type, name=None, **props):
    _check_gst_inited()
    factory = Gst.ElementFactory.find(type)
    if not factory:
        raise ElementNotFound(type)

    element = factory.create(name)
    if not element:
        raise GstException(name)

    return _set_props(element, **props)

_STATES = {
    'null': Gst.State.NULL,
    'ready': Gst.State.READY,
    'paused': Gst.State.PAUSED,
    'playing': Gst.State.PLAYING,
}

from functools import wraps
def get_gst_elements(f):
    @wraps(f)
    def wrapper(*args, **kwargs):
        args = [arg.gst_el if isinstance(arg, Element) else arg for arg in args[1:] ]
        return f(*args, **kwargs)
    return wrapper


class Element(object):
    def __init__(self, type, name=None, **props):
        self.gst_el = create_element(type, name, **props)

    def __getattr__(self, attr):
        attr = getattr(self.gst_el, attr)
        if type(attr) in (types.MethodType, FunctionInfo):
            return get_gst_elements(attr).__get__(self, Element)
        else:
            return attr

    def set_state(self, state):
        change = self.gst_el.set_state(_STATES[state])
        if change == Gst.StateChangeReturn.FAILURE:
            raise GstException(u'Unable to set element {0} to the [{0}] state.'.format(self, state))


class Bin(Element):
    def __init__(self, name=None, **props):
        _check_gst_inited()
        self.gst_el = self._get_gst_el()
        if not self.gst_el:
            raise GstException(name)
        _set_props(self.gst_el, **props)

    def _get_gst_el(self, name=None):
        return Gst.Bin()


class Pipeline(Bin):
    def _get_gst_el(self):
        return Gst.Pipeline()


def link(*items):
    pairs = zip(items, items[1:])
    for src, sink in pairs:
        if sink is None:
            return
        linked =  src.link(sink)
        if isinstance(linked, Gst.PadLinkReturn):
            if not (linked == Gst.PadLinkReturn.OK):
                raise Gst.LinkError()
        else:
            if not linked:
                raise Gst.LinkError()


def msg_str(msg):
    if msg.type == Gst.MessageType.ERROR:
        error, debug = msg.parse_error()
        return u"ERROR: {0}: {1} {2}".format(msg.src, error, debug)
    elif msg.type == Gst.MessageType.EOS:
        return u"End of stream"
    elif msg.type == Gst.MessageType.STATE_CHANGED:
        old, new, _ = msg.parse_state_changed()
        return u"STATE: {0}: {1} -> {2}".format(msg.src, old, new)
    elif msg.type == Gst.MessageType.DURATION_CHANGED:
        return u"Duration changed."
    else:
        return u"Unexpected message!"

def flags(namespace=None, *flags):
    ret = None
    namespace = getattr(Gst, namespace) if namespace is not None else Gst
    for flag in flags:
        if ret is None:
            ret = getattr(namespace, flag.upper())
        else:
            ret |= getattr(namespace, flag.upper())
    return ret

def gst_time_str(t):
    s, ns = divmod(t, 1000000000)
    m, s = divmod(s, 60)
    if m < 60:
        return "0:%02i:%02i.%i" % (m, s, ns)
    else:
        h, m = divmod(m, 60)
        return "%i:%02i:%02i.%i" % (h, m, s, ns)

def gst_times_str(*times):
    return [gst_time_str(t) for t in times]

import os
dots_dir = os.path.join(os.getcwd(), 'dots/')
dots_file = os.path.join(dots_dir, 'test')
#os.environ.update({'GST_DEBUG_DUMP_DOT_DIR': dots_dir })
def debug_graph(bin, name):
    if hasattr(bin, 'gst_el'):
        bin = bin.gst_el
    Gst.debug_bin_to_dot_file(bin, Gst.DebugGraphDetails.ALL, name)
    dots_dir = os.environ["GST_DEBUG_DUMP_DOT_DIR"]
    path = os.path.join(dots_dir, name)
    os.system("dot -Tpng -o {0}.png {0}.dot".format(path))

def _init_debug_graph(dots_dir=None):
    if dots_dir is None:
        dots_dir = os.path.join(os.getcwd(), 'dots/')
    elif dots_dir.startswith('/'):
        pass
    else:
        dots_dir = os.path.join(os.getcwd(), dots_dir)

    os.environ["GST_DEBUG_DUMP_DOT_DIR"] = dots_dir
    os.putenv('GST_DEBUG_DUMP_DIR_DIR', dots_dir)

debug_graph.init = _init_debug_graph


