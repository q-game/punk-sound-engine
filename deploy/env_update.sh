#!/bin/bash
# Удаляем старое виртуальное окружение, устанавливаем новое, накладываем патчи

cd `dirname "$0"`;
ENV_PATH='../../env/';

sudo apt-get -y install libzmq3-dev
sudo apt-get -y install jackd2
sudo apt-get -y install libffi-dev python-dev python-pip python-virtualenv
#sudo dpkg-reconfigure -p high jackd2 ;# for realtime and memory allocation

if [ $1 ]; then
    rm -r "$ENV_PATH";
    virtualenv --system-site-packages --python="python$1" "$ENV_PATH";
    $ENV_PATH/bin/pip install -r env.req;
else
   echo "Error: python version requires as first argument" 1>&2;
fi
