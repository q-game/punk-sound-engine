#see what sound 'devices' available
aplay -L

#add user to audio group
usermod -a -G audio theusername

jack_control exit
jack_control ds alsa
jack_control dps device plughw:CARD=Device,DEV=0
jack_control dps outchannels 8
jack_control eps realtime False
jack_control start

#In case of error: ALSA: Cannot open PCM device alsa_pcm for playback. Falling back to capture-only mode
#dont forget set correct device
jack_control dps device plughw:CARD=Device,DEV=0

#In case of error: Failed to acquire device name : Audio0 error : A handler is already registered for /org/freedesktop/ReserveDevice1/Audio0
#kill jackdbus and start again
jack_control exit



#jack channels
#1,2 - front
#7,8 - surround
#3,4 - center/bass
#5,6 - back



