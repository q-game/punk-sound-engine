from gi.repository import Gst
import sys
import threading
from multiprocessing import Process
from os import path
import os
import glob

from utils import Element, Pipeline, Bin
from utils import link, flags, msg_str
from utils import debug_graph
debug_graph.init()

class Sound(Process):
    def __init__(self, file_path, **kwargs):
        self.sound_name = path.basename(file_path).split('.')[0]
        Process.__init__(self, **kwargs.update({'name': self.sound_name}))
        self.file_path = file_path

    def make_pipeline(self):
        Gst.init()
        self.pipeline = Pipeline(self.name)

        self.el_src = Element('filesrc', 'src', location=file_path)
        self.el_parse = Element('wavparse', 'parse')
        self.el_convert = Element('audioconvert', 'convert')
        self.el_resample = Element('audioresample', 'resample')

        self.pipeline.add(self.el_src, self.el_parse, self.el_convert, self.el_resample)
        link(self.el_src, self.el_parse, self.el_convert, self.el_resample)




