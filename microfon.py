#! ../env/bin/python
# encode: utf-8
from os import path
import signal
import sys
import sys

from gi.repository import Gst

from utils import Element, Pipeline
from utils import link, flags, msg_str
#from utils import debug_graph
#debug_graph.init()

class MicroAmp(object):
    def __init__(self, volume):
        volume = float(volume)
        if volume > 3:
            raise Exception('To high volume! Change source code if you really need it.')

        self.pipeline = Pipeline()
        #set connect=0 and use sript in case autoconnect dont work
        self.el_src = Element('jackaudiosrc', 'microfon', connect=2, client_name='microfon_amplifier')
        self.el_volume = Element('volume', 'volume', volume=volume)
        self.el_sink = Element('jackaudiosink', 'speakers', connect=2, client_name='microfon_amplifier')

        self.pipeline.add(self.el_src)
        self.pipeline.add(self.el_volume)
        self.pipeline.add(self.el_sink)

        link(self.el_src, self.el_volume)
        link(self.el_volume, self.el_sink)

    def play(self):
        pipeline = self.pipeline

        #debug_graph(pipeline, 'pre')
        pipeline.set_state('playing')
        #debug_graph(pipeline, 'post')

        bus = pipeline.get_bus()

        def term_handler(*args):
            pipeline.set_state('null')
            raise SystemExit('Terminated by signal.')
        signal.signal(signal.SIGTERM, term_handler)

        #USE THIS in case you need volue control
        #def usr1_handler(*args):
            #TODO: volume control
            #pipeline.set_state('playing')
        #signal.signal(signal.SIGUSR1, usr1_handler)

        #def usr2_handler(*args):
            #TODO: volume control
            #pipeline.set_state('paused')
        #signal.signal(signal.SIGUSR2, usr2_handler)


        terminate = False
        while not terminate:
            msg = bus.timed_pop_filtered(
                10*Gst.MSECOND,
                flags('MessageType', *'error eos'.split())
            )
            if msg is None:
                continue

            if msg.type & flags('MessageType', 'error'):
                pipeline.terminate = True

        pipeline.set_state('null')


def main():
    Gst.init(sys.argv)
    MicroAmp(2).play()

if __name__ == "__main__":
    main()

