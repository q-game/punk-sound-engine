#! ../env/bin/python
# encode: utf-8
from os import path
import signal
import sys
import sys

from gi.repository import Gst

from utils import Element, Pipeline
from utils import link, flags, msg_str
#from utils import debug_graph
#debug_graph.init()

class Sound(object):
    def __init__(self, file_path, volume, id):
        self.name = path.basename(file_path).split('.')[0]
        self.id = str(id) if id else ''
        self.full_name = self.name + ('_'+self.id if self.id else '')
        volume = float(volume)
        if volume > 3:
            raise Exception('To high volume! Change source code if you really need it.')

        self.pipeline = Pipeline()
        self.el_src = Element('filesrc', 'src', location=file_path)
        self.el_parse = Element('wavparse', 'parse')
        self.el_queue = Element('queue', 'queue')
        self.el_volume = Element('volume', 'volume', volume=volume)
        self.el_convert = Element('audioconvert', 'convert')
        self.el_resample = Element('audioresample', 'resample')
        self.el_sink = Element('jackaudiosink', 'chnl', connect=0, client_name=self.full_name)

        self.pipeline.add(self.el_src)
        self.pipeline.add(self.el_parse)
        self.pipeline.add(self.el_queue)
        self.pipeline.add(self.el_volume)
        self.pipeline.add(self.el_convert)
        self.pipeline.add(self.el_resample)
        self.pipeline.add(self.el_sink)

        link(self.el_src, self.el_parse)
        link(self.el_parse, self.el_queue)
        link(self.el_queue, self.el_volume)
        #link(self.el_parse, self.el_volume)
        link(self.el_volume, self.el_convert)
        link(self.el_convert, self.el_resample)
        link(self.el_resample, self.el_sink)

    def play(self, loop=False, paused=False):
        pipeline = self.pipeline

        #debug_graph(pipeline, 'pre')
        pipeline.set_state('playing' if not paused else 'paused')
        #debug_graph(pipeline, 'post')

        bus = pipeline.get_bus()

        def term_handler(*args):
            pipeline.set_state('null')
            raise SystemExit('Terminated by signal.')
        signal.signal(signal.SIGTERM, term_handler)

        def usr1_handler(*args):
            pipeline.set_state('playing')
        signal.signal(signal.SIGUSR1, usr1_handler)

        def usr2_handler(*args):
            pipeline.set_state('paused')
        signal.signal(signal.SIGUSR2, usr2_handler)


        terminate = False
        while not terminate:
            msg = bus.timed_pop_filtered(
                1000*Gst.MSECOND,
                flags('MessageType', 'eos')
            )
            if msg is None:
                continue

            if msg.type & flags('MessageType', 'eos'):
                if loop:
                    pipeline.seek_simple(
                        Gst.Format.TIME,
                        flags('SeekFlags', 'flush', 'key_unit'),
                        0*Gst.SECOND
                    )
                else:
                    terminate = True

        pipeline.set_state('null')


def main():
    sound_file = sys.argv[1]
    volume = sys.argv[2]
    loop = bool(int(sys.argv[3]))
    paused = bool(int(sys.argv[4]))
    if len(sys.argv) > 5:
        id = sys.argv[5]
    else:
        id = None

    Gst.init(sys.argv)
    Sound(sound_file, volume, id).play(loop, paused)

if __name__ == "__main__":
    main()

