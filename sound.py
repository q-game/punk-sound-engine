# encode: utf-8
from subprocess import Popen
from os import path
import os
import signal
import collections
import itertools
import time
import shlex
import psutil

import logging
log = logging.getLogger('BAL')

import jack

jackcli = jack.Client('SoundRouter')

class SoundError(Exception): pass

class Sound(object):
    def __init__(self, file_path, routes=[], volume=0.1, loop=False, id=None):
        if not path.isfile(file_path):
            log.error(u"No such file: {0}".format(file_path))
            raise OSError(os.errno.ENOENT, os.strerror(os.errno.ENOENT))

        self.name = path.basename(file_path).split('.')[0]
        self.id = str(id) if id else ''
        self.full_name = self.name + ('_'+self.id if self.id else '')
        self.file_path = file_path
        self.volume = volume
        self.routes = routes
        self.loop = loop
        self.paused = False

    def play(self, routes=None, volume=None, loop=None, paused=None):
        if volume:
            self.volume = volume
        if routes:
            self.routes = routes
        if loop:
            self.loop = loop
        if paused:
            self.paused = paused

        if self.is_played():
            raise SoundError(u"Sound already played.")

        self.stop()
        pargs = ['./play.py', self.file_path, str(volume or self.volume), str(int(loop or self.loop)), str(int(paused or self.paused))]
        if self.id:
            pargs.append(self.id)

        self.proc = Popen(pargs)
        for _ in range(0, 500):
            if self.is_played():
                break
            time.sleep(0.05)
        else:
            self.stop()
            raise SoundError(u"Can't find ports!")

        self.route(routes)
        return self

    def resume(self):
        if getattr(self, 'proc', None):
            self.proc.send_signal(signal.SIGUSR1)


    def pause(self):
        if getattr(self, 'proc', None):
            self.proc.send_signal(signal.SIGUSR2)

    def stop(self):
        if getattr(self, 'proc', None):
            self.proc.terminate()
            self.proc.wait()
            del self.proc

    def route(self, routes=None):
        for src, sink in self._gen_ports(routes):
            self.connect(src, sink)

    def unroute(self, routes=None):
        for src, sink in self._gen_ports(routes):
            self.disconnect(src, sink)

    def _gen_ports(self, routes):
        if routes is None:
            routes = self.routes

        for route in routes:
            srcs, sinks = route
            if not isinstance(srcs, collections.Iterable):
                srcs = (srcs,)

            if not isinstance(sinks, collections.Iterable):
                sinks = (sinks,)

            for src, sink in itertools.product(srcs, sinks):
                yield src, sink


    def connect(self, src, sink):
        src_port = "{0}:out_chnl_{1}".format(self.full_name, src)
        sink_port = "system:playback_{0}".format(sink)
        try:
            jackcli.connect(src_port, sink_port)
        except jack.JackError as err:
            if 'already exists' in err.message:
                log.info(err.message)
            elif 'Error connecting' in err.message:
                log.info(err.message)
            else:
                raise err

    def disconnect(self, src, sink):
        src_port = "{0}:out_chnl_{1}".format(self.full_name, src)
        sink_port = "system:playback_{0}".format(sink)
        try:
            jackcli.disconnect(src_port, sink_port)
        except jack.JackError as err:
            if "Couldn't disconnect" in err.message:
                log.info(err.message)
            else:
                raise err

    def is_played(self):
        return bool(jackcli.get_ports("{0}:out_chnl*".format(self.full_name)))

class NotInPlaylist(SoundError): pass

UNIMPORTANT_SOUNDS = set([
     'nepravilnoe_poleno',
     'pravilnoe_poleno',
     'bolshaya_visota',
     'bolshaya_visota_eng',
     'cel_ne_ustanovlena',
     'cel_ne_ustanovlena_eng',
     'moyka',
     'moyka_eng',
     'malaya_visota',
     'malaya_visota_eng',
     'naberite_visote_eng',
     'naberite_visotu',
     'ne_pochinen_fuzelyazh',
     'ne_pochinen_fuzelyazh_eng',
     'net_mayaka',
     'net_mayaka_eng',
])

class Playlist(object):
    sounds = dict()

    def add(self, file_path, id=None):
        try:
            sound = Sound(file_path, id=id)
        except OSError:
            return

        if sound.full_name not in self.sounds:
            self.sounds[sound.full_name] = sound
        else:
            log.error(u"File already in playlist: {0}".format(file_path))

    def get_sound(self, full_name):
        sound = self.sounds.get(full_name, None)
        if sound is None:
            raise NotInPlaylist(u"Sound {0} not in playlist.".format(full_name))

        return sound

    def stop_all(self):
        for full_name, sound in self.sounds.items():
            sound.stop()

    def play(self, full_name, routes=None, volume=None, loop=None, paused=None):
        sound = self.get_sound(full_name)
        sound.play(routes, volume, loop, paused)

    def check_cpu(self):
        for full_name, sound in self.sounds.items():
            if sound.loop or (sound.name in UNIMPORTANT_SOUNDS):
                if not getattr(sound, 'proc', None):
                    print "No proc attr for sound: {0}".format(full_name)
                    sound.stop()
                    continue

                p = psutil.Process(sound.proc.pid)
                cpu = p.cpu_percent(0.1)
                if cpu > 40:
                    print "CPU to high! Autocheck. PID: {0} CPU: {1} FILE: {2} ".format(sound.proc.pid, cpu, full_name)
                    sound.stop()
                    sound.play()


    def stop(self, full_name):
        sound = self.get_sound(full_name)
        sound.stop()

    def resume(self, full_name):
        sound = self.get_sound(full_name)
        sound.resume()

    def pause(self, full_name):
        sound = self.get_sound(full_name)
        sound.pause()

    def route(self, full_name, routes):
        sound = self.get_sound(full_name)
        sound.route(routes)

    def unroute(self, full_name, routes):
        sound = self.get_sound(full_name)
        sound.unroute(routes)


